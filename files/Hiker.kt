package hiker
 
fun anagrams(word: String): List<String> {
    val lowerCaseWord = word.toLowerCase() // Deprecated function, use .lowercase()
    return anagramsRecursive(lowerCaseWord, 0, word.length-1)
}

fun anagramsRecursive(word: String, start: Int, end: Int): List<String> {
    var anagrams: MutableSet<String> = mutableSetOf<String>()
    
    if (start >= end) {
        anagrams.add(word)
    }
    else for (index in start..end) {
        val anagram: String = swap(word, start, index)
        anagrams.addAll(anagramsRecursive(anagram, start+1, end))
    }
    
    return anagrams.toList()
}    
    
fun swap(word: String, index1: Int, index2: Int): String {
    var wordArray: CharArray = word.toCharArray()
    
    val temp: Char = wordArray[index1]
    wordArray[index1] = wordArray[index2]
    wordArray[index2] = temp
    
    return String(wordArray)
}