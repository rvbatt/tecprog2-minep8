// [X] See please-help.txt 

package hiker /*[X]*/

import io.kotlintest.specs.StringSpec
import io.kotlintest.shouldBe
import io.kotlintest.shouldNotBe

class HikerTest /*[X]*/ : StringSpec() {

    init {
        "Empty String Test" {
            hiker.anagrams("") shouldBe listOf("")
         }
        
        "Single Character Test" {
            hiker.anagrams("a") shouldBe listOf("a")
         }
                
        "Two Characters Test" {
            hiker.anagrams("ab") shouldBe listOf("ab", "ba")
        }
               
        "Three Charactes Test" {
            hiker.anagrams("abc") shouldBe listOf("abc", "acb", "bac", "bca", "cba", "cab")
        } 
                
        "Problem Example Test" {
            hiker.anagrams("biro") shouldBe listOf(
                "biro", "bior", "brio", "broi", "bori", "boir",
                "ibro", "ibor", "irbo", "irob", "iorb", "iobr",
                "ribo", "riob", "rbio", "rboi", "robi", "roib",
                "oirb", "oibr", "orib", "orbi", "obri", "obir"
            )
        }
        
        "No Repeated Anagrams Test" {
            hiker.anagrams("ada") shouldBe listOf("ada", "aad", "daa")
        }
        
        "Same Letter Test" {
            hiker.anagrams("aaa") shouldBe listOf("aaa")
        }
        
        "Uppercase Test" {
            hiker.anagrams("Ada") shouldBe listOf("ada", "aad", "daa")
        }
        
        "Accented Letters Test" {
            hiker.anagrams("âéõ") shouldBe listOf("âéõ", "âõé", "éâõ", "éõâ", "õéâ", "õâé")
        }
        
        "Accented and Uppercase Letters Test" {
            hiker.anagrams("ARÁ") shouldBe listOf("ará", "aár", "raá", "ráa", "ára", "áar")
        }
        
        "Whitespace Test" {
            hiker.anagrams("a c") shouldBe listOf("a c", "ac ", " ac", " ca", "c a", "ca ")
        }
        
        "Newline Test" {
            hiker.anagrams("a\nc") shouldBe listOf("a\nc", "ac\n", "\nac", "\nca", "c\na", "ca\n")
        }
    }
}
