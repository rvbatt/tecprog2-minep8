## This a copy of [your cyber-dojo exercise](https://cyber-dojo.org/kata/edit/TXGjRu):
- Exercise: `Anagrams`
- Language & test-framework: `Kotlin, Kotlintest`

## Sobre
Este repositório é uma cópia do exercício realizado no site [cyber-dojo](https://cyber-dojo.org) para o MiniEP8 de Técnicas de Programação 2. O site apresenta um sistema de versionamento próprio, além de uma ferramenta que lhe permite prever o resultado dos testes antes de executá-los: Verde significa que é esperado um sucesso, Vermelho significa que o teste irá falhar e Amarelo indica falha na compilação.

### Instruções
- O código da sessão utilizada é (*case sensitive*): `TXGjRu`
- Os testes foram feitos no arquivo `HikerTest.kt` e o código fonte em `Hiker.kt`
- Para visualizar as diferentes versões, clique nos ícones coloridos na parte superior da tela.

## Experiência com TDD
Procurei seguir os passos do TDD, sempre escrevendo os testes antes do código em si, começando com testes mais simples e aumentando a complexidade, além de tratar de *corner cases*. Ao escrever um novo teste, executava-o com a expectativa dele não passar, utilizando a ferramenta de previsão do site, e depois procurava fazer a implementação mínima para que ele ficasse verde.

Meu maior problema com o uso de Test Driven Development foram os primeiros dois ou três ciclos, pois como estava realizando testes muito simples e a intenção do modelo de desenvolvimento é fazer o mínimo para que os testes passassem, a sensação é que as primeiras implementações da função parecem ser quase inúteis. Para o primeiro teste, por exemplo, eu fiz com que a função apenas retornasse a entrada, o que é o comportamento esperado no caso testado, mas era óbvio que isso seria extremamente modificado em testes posteriores.

Fora isso, o uso dos testes automatizados me pareceu extremamente útil, por fornecer uma "rede de proteção" que garantia que a função estava funcionando corretamente.
